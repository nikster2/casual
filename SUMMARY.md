# Summary

### General
* [Introduction](README.md)
* [Build](build.md)
* [Licence](license.md)

### Make
* [casual-make](make/readme.md)

----

### *Middleware*
* [Overview](middleware/readme.md)
* [Administration](middleware/administration/documentation/api.md)
* [Binding](middleware/binding/readme.md)
* [Buffer](middleware/buffer/readme.md)
   * [Field](middleware/buffer/documentation/field.md)
   * [Octet](middleware/buffer/documentation/octet.md)
   * [Order](middleware/buffer/documentation/order.md)
   * [String](middleware/buffer/documentation/string.md)
* Common
   * [Argument](middleware/common/documentation/arguments.md)
   * [Log](middleware/common/documentation/log.md)
* [Configuration](middleware/configuration/example/readme.md)
* [Domain](middleware/domain/readme.md)
   * [Api](middleware/domain/documentation/api.md)
* [Gateway](middleware/gateway/documentation/api.md)
   * [Protocol](middleware/gateway/documentation/protocol/protocol.md)
   * [Semantics](middleware/gateway/documentation/semantics.md)
* [http](middleware/http/documentation/outbound/configuration.operation.md)
* [Service](middleware/service/documentation/api.md)
* Tools - Build
   * [Server](middleware/tools/documentation/build/server.md)
   * [Resource](middleware/tools/documentation/build/resource/proxy.md)
* [Transaction](middleware/transaction/documentation/api.md)
  * [Interdomain transaction](middleware/transaction/documentation/interdomain-transaction.md)

----

### Thirdparty
* [Overview](thirdparty/readme.md)
   * [Database](thirdparty/database/include/sql/readme.md)
   * [Setup](thirdparty/setup/readme.md)

### Webapp
* [Requirements web-gui](documentation/requirement/web-gui.md)
* [Webapp](webapp/README.md)

### Examples
* [Overview](middleware/example/domain/readme.md)
   * [Single](middleware/example/domain/single/minimal/readme.md)
   * [Multiple Minimal](middleware/example/domain/multiple/minimal/readme.md)
   * [Multiple Medium](middleware/example/domain/multiple/medium/readme.md)
   * [IDE](middleware/example/ide/readme.md)

* [Docker](docker/README.md)
