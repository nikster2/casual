# describe `casual CLI`

``` 
  describe  0..1  (<service> [json,yaml,xml,ini]) [1..2]
        service describer
        
           * service   name of the service to to describe
           * [format]  optional format of the output, if absent a _CLI format_ is used.
        
           attention: the service need to be a casual aware service
```
