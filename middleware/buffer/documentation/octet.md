# octet

## overview

This implementation contain a C-interface that offers functionality to set and get data into and from a buffer and are just a tiny addition to the XATMI-standard x-octet-buffer ("X-OCTET")

### [development](octet.development.md)

### [maintenance](octet.maintenance.md)
