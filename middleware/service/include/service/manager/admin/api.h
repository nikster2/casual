//!
//! Copyright (c) 2018, The casual project
//!
//! This software is licensed under the MIT license, https://opensource.org/licenses/MIT
//!

#pragma once


#include "service/manager/admin/managervo.h"

namespace casual
{
   namespace service
   {
      namespace manager
      {
         namespace admin
         {
            namespace api
            {
               inline namespace v1
               {
                  StateVO state();
                  
               } // v1

            } // api
         } // admin
      } // manager  
   } // service
} // casual

